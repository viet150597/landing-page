import { NgModule } from "@angular/core";
import { CoreDragDropDirective } from "./core-drag-drop.directive";

@NgModule({
  declarations: [CoreDragDropDirective],
  exports: [CoreDragDropDirective]
})
export class CoreDragDropModule { }
