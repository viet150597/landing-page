import { DOCUMENT } from '@angular/common';
import { Directive, Inject, OnInit, Renderer2 } from '@angular/core';
import { SelectedElementService } from '@ladi/util';
import { fromEvent, Observable, of } from 'rxjs';
import { map, switchMap, take, takeUntil, tap } from 'rxjs/operators';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[coreDragDrop]',
})
export class CoreDragDropDirective implements OnInit {

  mouseUp$ = fromEvent(this.doc, 'mouseup').pipe(
    take(1)
  );
  constructor(
    @Inject(DOCUMENT) private doc: any,
    private renderer2: Renderer2,
    private selectedElementService: SelectedElementService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.mouseDownDocumentEvent().pipe(switchMap((e: MouseEvent | any) => {
      return this.handleDragableElement(e);
    })).subscribe();
  }

  handleDragableElement(event: MouseEvent): Observable<unknown> {
    const element = event.target as HTMLElement;
    if (element.getAttribute('ladi-draggable')) {
      // continue process mousedown event
      return of(element).pipe(
        tap((element) => {
          this.setStyleDragElement(element);
        }),
        switchMap(() => {
          return this.handleDragging(event, element);
        }),
        takeUntil(this.mouseUp$.pipe(tap(() => {
          this.removeStyleDragElement(element);
          this.removeStyleOldSelectedElement(element);
          this.handleSelectedElement(element);
        })))
      )
    }
    return of(true).pipe(tap(() => {
      //handle when click outside đraggable element
      if (element.nodeName === 'HTML') {
        this.removeStyleOldSelectedElement(element);
        this.selectedElementService.setSelectedElement(undefined);
      }
    }));
  }

  handleDragging(event: MouseEvent, elementSelected: HTMLElement): Observable<unknown> {
    /* handle mouseup of selected element
     *  The effect of obseverble is unsubcrible event mousemove when user mouseup
     */
    const mouseMove$ = fromEvent(this.doc, 'mousemove');
    return mouseMove$.pipe(
      map((eMouseMove: MouseEvent | any) => {
        // caculate position top left
        const left = eMouseMove.pageX - event.offsetX;
        const top = eMouseMove.pageY - event.offsetY;
        return { left, top };
      }),
      tap(({ left, top }) => {
        this.renderer2.setStyle(elementSelected, 'left', left + 'px');
        this.renderer2.setStyle(elementSelected, 'top', top + 'px');
      }),
      takeUntil(this.mouseUp$)
    )
  }

  handleSelectedElement(element: HTMLElement) {
    this.selectedElementService.setSelectedElement(element);
    this.renderer2.addClass(element, 'selected');
  }

  removeStyleOldSelectedElement(element: HTMLElement) {
    const isRemoveClassSelectedElement = this.selectedElementService.selectedElement &&
      this.selectedElementService.selectedElement !== element;
    if (isRemoveClassSelectedElement) {
      this.renderer2.removeClass(this.selectedElementService.selectedElement, 'selected');
    }
  }

  mouseDownDocumentEvent() {
    return fromEvent(this.doc, 'mousedown');
  }

  setStyleDragElement(element: HTMLElement) {
    this.renderer2.addClass(element, 'dragging');
  }

  removeStyleDragElement(element: HTMLElement) {
    this.renderer2.removeClass(element, 'dragging');
  }
}
