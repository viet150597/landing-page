import { Injectable, Renderer2 } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SelectedElementService {
  selectedElement?: HTMLElement;
  private selectedElementSubject = new BehaviorSubject<HTMLElement | undefined>(undefined);
  selectedElement$ = this.selectedElementSubject.asObservable();

  constructor() {
    this.selectedElementSubject.pipe(tap(ele => this.selectedElement = ele)).subscribe()
  }

  setSelectedElement(element: HTMLElement | undefined) {
    this.selectedElementSubject.next(element);
  }

}
