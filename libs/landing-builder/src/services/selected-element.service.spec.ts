/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SelectedElementService } from './selected-element.service';

describe('Service: SelectedElement', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectedElementService]
    });
  });

  it('should ...', inject([SelectedElementService], (service: SelectedElementService) => {
    expect(service).toBeTruthy();
  }));
});
