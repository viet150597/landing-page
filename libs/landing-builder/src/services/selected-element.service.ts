import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SelectedElementService {

  private selectedElementSubject = new BehaviorSubject<HTMLElement | undefined>(undefined);
  selectedElement$ = this.selectedElementSubject.asObservable();
  oldSelectedElement?: HTMLElement;

  setSelectedElement(element: HTMLElement) {
    if (this.selectedElementSubject.getValue()) {
      this.oldSelectedElement = this.selectedElementSubject.getValue();
    }
    this.selectedElementSubject.next(element);
  }

}
