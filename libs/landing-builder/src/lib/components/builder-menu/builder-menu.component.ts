import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'builder-menu',
  templateUrl: './builder-menu.component.html',
  styleUrls: ['./builder-menu.component.scss']
})
export class BuilderMenuComponent {
  items = [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ]
  constructor(
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) { }

  async addElement() {
    const btn = (await import('../button/button.component')).ButtonComponent
    const componentF = this.cfr.resolveComponentFactory(btn);
    this.vcr.createComponent(componentF);
  }

}
