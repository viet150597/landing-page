import { Component } from '@angular/core';

@Component({
  selector: 'builder-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  color = '';
  backgroundColor = 'pink';
  width = 'auto';
  height = 'auto';

}
