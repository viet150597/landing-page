import { DestroyService } from '../../../../util/src/lib/services/destroy/destroy.service';
import { DOCUMENT } from '@angular/common';
import { Directive, ElementRef, Renderer2, OnInit, Inject } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { SelectedElementService } from '../../services/selected-element.service';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[cdkDragDrop]',
  providers: [
    DestroyService
  ]
})
export class CdkDragDropDirective {
  // destroy$: Subject<boolean> = new Subject();
  // element: HTMLElement;
  // selected?: HTMLElement;
  // constructor(
  //   private elementRef: ElementRef,
  //   private renderer2: Renderer2,
  //   @Inject(DOCUMENT) private doc: any,
  //   private selectedElementService: SelectedElementService,
  //   @Inject(DestroyService) destroy$: DestroyService
  // ) {
  //   this.element = this.elementRef.nativeElement;
  // }

  // ngOnInit(): void {
  //   this.initDragDrop();
  //   this.handleSelectedElement();
  // }

  // initDragDrop() {
  //   fromEvent(this.doc, 'mousedown').pipe(tap(console.log)).subscribe()
  //   const mouseDown$ = fromEvent(this.element, 'mousedown');
  //   const mouseUp$ = fromEvent(this.doc, 'mouseup').pipe(
  //     tap(() => {
  //       this.removeDragElement();
  //     }),
  //     take(1)
  //   );
  //   const mouseMove$ = fromEvent(this.doc, 'mousemove').pipe(takeUntil(mouseUp$));
  //   mouseDown$
  //     .pipe(
  //       tap(() => {
  //         this.setDragElement();
  //         this.selectedElementService.setSelectedElement(this.element);
  //       }),
  //       switchMap((eMouseDown: MouseEvent | any) => {
  //         return mouseMove$.pipe(
  //           map((eMouseMove: MouseEvent | any) => {
  //             // caculate position top left
  //             const left = eMouseMove.pageX - eMouseDown.offsetX;
  //             const top = eMouseMove.pageY - eMouseDown.offsetY;
  //             return { left, top };
  //           })
  //         )
  //       }),
  //       tap(({ left, top }) => {
  //         this.renderer2.setStyle(this.element, 'left', left + 'px');
  //         this.renderer2.setStyle(this.element, 'top', top + 'px');
  //       }),
  //       takeUntil(this.destroy$)
  //     )
  //     .subscribe();
  // }

  // // handleSelectElement() {
  // //   this.selected.subscribe(element => {
  // //     this.renderer2.addClass(element, 'selected');
  // //     if (this.oldSelectedElement) {
  // //       this.renderer2.removeClass(this.oldSelectedElement, 'selected');

  // //     }
  // //     this.oldSelectedElement = element;

  // //   })
  // // }
  // handleSelectedElement() {
  //   this.selectedElementService.selectedElement$.pipe(
  //     takeUntil(this.destroy$)
  //   ).subscribe(element => {
  //     if (element) {
  //       this.renderer2.addClass(this.element, 'selected');
  //     }
  //     if (this.selectedElementService.oldSelectedElement &&
  //       this.selectedElementService.oldSelectedElement !== element) {
  //       this.renderer2.removeClass(this.selectedElementService.oldSelectedElement, 'selected');
  //     }
  //   })

  // }

  // setStyleDragElement() {
  //   this.renderer2.addClass(this.element, 'dragging');
  // }

  // removeStyleDragElement() {
  //   this.renderer2.removeClass(this.element, 'dragging');
  // }
}
