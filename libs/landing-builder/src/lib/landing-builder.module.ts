import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingBuilderComponent } from './landing-builder.component';
import { ButtonComponent } from './components/button/button.component';
import { BuilderMenuComponent } from './components/builder-menu/builder-menu.component';
import { BuilderQuickEditorComponent } from './components/builder-quick-editor/builder-quick-editor.component';

const COMPONENTS = [
  LandingBuilderComponent,
  ButtonComponent,
  BuilderMenuComponent,
  BuilderQuickEditorComponent
];

const DIRECTIVES = [

]

@NgModule({
  declarations: [
    ...COMPONENTS,
    // ...DIRECTIVES
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ...COMPONENTS,
    // ...DIRECTIVES
  ]
})
export class LandingBuilderModule { }
