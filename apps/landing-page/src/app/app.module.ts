import { LandingBuilderModule } from '@ladi/landing-builder';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreDragDropModule } from '@ladi/util'
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, LandingBuilderModule, CoreDragDropModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
